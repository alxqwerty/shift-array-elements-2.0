﻿using System;

namespace ShiftArrayElements
{
    public static class Shifter
    {
        public static int[] Shift(int[] source, int[] iterations)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (iterations is null)
            {
                throw new ArgumentNullException(nameof(iterations));
            }

            int[] tempArray = new int[source.Length];

            for (int i = 0; i < iterations.Length; i++)
            {
                if (i % 2 == 0 || i == 0)
                {
                    int index = 0;

                    while (index < iterations[i])
                    {
                        for (int j = 0; j < source.Length - 1; j++)
                        {
                            tempArray[j] = source[j + 1];
                        }

                        tempArray[^1] = source[0];

                        Array.Copy(tempArray, source, tempArray.Length);
                        index++;
                    }
                }

                if (i % 2 != 0)
                {
                    int index = 0;

                    while (index < iterations[i])
                    {
                        for (int j = 0; j < source.Length - 1; j++)
                        {
                            tempArray[j + 1] = source[j];
                        }

                        tempArray[0] = source[^1];
                        Array.Copy(tempArray, source, tempArray.Length);
                        index++;
                    }                    
                }
            }

            return source;
       }
    }
}
