﻿using System;

namespace ShiftArrayElements
{
    public static class EnumShifter
    {
        public static int[] Shift(int[] source, Direction[] directions)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (directions is null)
            {
                throw new ArgumentNullException(nameof(directions));
            }

            for (int i = 0; i < directions.Length; i++)
            {
                if (directions[i] != Direction.Left && directions[i] != Direction.Right)
                {
                    throw new InvalidOperationException(nameof(directions));
                }
            }

            for (int i = 0; i < directions.Length; i++)
            {
                int[] tempArray = new int[source.Length];

                switch (directions[i])
                {
                    case Direction.Left:
                        {
                            for (int j = 0; j < source.Length - 1; j++)
                            {
                                tempArray[j] = source[j + 1];
                            }

                            tempArray[^1] = source[0];

                            source = tempArray;
                            break;
                        }

                    case Direction.Right:
                        {
                            for (int j = 1; j < source.Length; j++)
                            {
                                tempArray[j] = source[j - 1];
                            }

                            tempArray[0] = source[^1];
                            source = tempArray;
                            break;
                        }

                    default:
                        throw new InvalidOperationException($"Incorrect {directions[i]} enum value.");
                }
            }

            return source;
        }
    }
}